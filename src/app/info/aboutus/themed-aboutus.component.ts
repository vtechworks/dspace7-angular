import { Component } from '@angular/core';
import { ThemedComponent } from '../../shared/theme-support/themed.component';
import { AboutusComponent } from './aboutus.component';

/**
 * Themed wrapper for PrivacyComponent
 */
@Component({
  selector: 'ds-themed-aboutus',
  styleUrls: [],
  templateUrl: '../../shared/theme-support/themed.component.html',
})
export class ThemedAboutusComponent extends ThemedComponent<AboutusComponent> {
  protected getComponentName(): string {
    return 'AboutusComponent';
  }

  protected importThemedComponent(themeName: string): Promise<any> {
    return import(`../../../themes/${themeName}/app/info/aboutus/aboutus.component`);
  }

  protected importUnthemedComponent(): Promise<any> {
    return import(`./aboutus.component`);
  }

}
