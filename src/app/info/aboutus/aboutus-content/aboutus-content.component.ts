import { Component } from '@angular/core';

@Component({
  selector: 'ds-aboutus-content',
  templateUrl: './aboutus-content.component.html',
  styleUrls: ['./aboutus-content.component.scss']
})
/**
 * Component displaying the contents of the aboutus Statement
 */
export class AboutusContentComponent {
}
