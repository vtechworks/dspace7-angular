import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { I18nBreadcrumbResolver } from '../core/breadcrumbs/i18n-breadcrumb.resolver';
import { PRIVACY_PATH, END_USER_AGREEMENT_PATH, FEEDBACK_PATH, ABOUTUS_PATH, VTHELP_PATH, VTPOLICIES_PATH } from './info-routing-paths';
import { ThemedEndUserAgreementComponent } from './end-user-agreement/themed-end-user-agreement.component';
import { ThemedAboutusComponent } from './aboutus/themed-aboutus.component';
import { ThemedVTHelpComponent } from './vthelp/themed-vthelp.component';
import { ThemedVTPoliciesComponent } from './vtpolicies/themed-vtpolicies.component';
import { ThemedPrivacyComponent } from './privacy/themed-privacy.component';
import { ThemedFeedbackComponent } from './feedback/themed-feedback.component';
import { FeedbackGuard } from '../core/feedback/feedback.guard';
import { environment } from '../../environments/environment';



const imports = [
  RouterModule.forChild([
    {
      path: FEEDBACK_PATH,
      component: ThemedFeedbackComponent,
      resolve: { breadcrumb: I18nBreadcrumbResolver },
      data: { title: 'info.feedback.title', breadcrumbKey: 'info.feedback' },
      canActivate: [FeedbackGuard]
    }
  ])
];

  if (environment.info.enableEndUserAgreement) {
    imports.push(
      RouterModule.forChild([
        {
          path: END_USER_AGREEMENT_PATH,
          component: ThemedEndUserAgreementComponent,
          resolve: { breadcrumb: I18nBreadcrumbResolver },
          data: { title: 'info.end-user-agreement.title', breadcrumbKey: 'info.end-user-agreement' }
        }
      ]));
  }
if (environment.info.enableAboutusStatement) {
  imports.push(
    RouterModule.forChild([
      {
        path: ABOUTUS_PATH,
        component: ThemedAboutusComponent,
        resolve: { breadcrumb: I18nBreadcrumbResolver },
        data: { title: 'info.aboutus.title', breadcrumbKey: 'info.aboutus' }
      }
    ]));
}
if (environment.info.enableVTHelpStatement) {
  imports.push(
    RouterModule.forChild([
      {
        path: VTHELP_PATH,
        component: ThemedVTHelpComponent,
        resolve: { breadcrumb: I18nBreadcrumbResolver },
        data: { title: 'info.vthelp.title', breadcrumbKey: 'info.vthelp' }
      }
    ]));
}

if (environment.info.enableVTPoliciesStatement) {
  imports.push(
    RouterModule.forChild([
      {
        path: VTPOLICIES_PATH,
        component: ThemedVTPoliciesComponent,
        resolve: { breadcrumb: I18nBreadcrumbResolver },
        data: { title: 'info.vtpolicies.title', breadcrumbKey: 'info.vtpolicies' }
      }
    ]));
}

  if (environment.info.enablePrivacyStatement) {
    imports.push(
      RouterModule.forChild([
        {
          path: PRIVACY_PATH,
          component: ThemedPrivacyComponent,
          resolve: { breadcrumb: I18nBreadcrumbResolver },
          data: { title: 'info.privacy.title', breadcrumbKey: 'info.privacy' }
        }
      ]));
  }

@NgModule({
  imports: [
    ...imports
  ]
})
/**
 * Module for navigating to components within the info module
 */
export class InfoRoutingModule {
}
