import { getInfoModulePath } from '../app-routing-paths';

export const END_USER_AGREEMENT_PATH = 'end-user-agreement';
export const ABOUTUS_PATH = 'aboutus';
export const VTHELP_PATH = 'vthelp';
export const VTPOLICIES_PATH = 'vtpolicies';
export const PRIVACY_PATH = 'privacy';
export const FEEDBACK_PATH = 'feedback';

export function getEndUserAgreementPath() {
    return getSubPath(END_USER_AGREEMENT_PATH);
}
export function getAboutusPath() {
  return getSubPath(ABOUTUS_PATH);
}

export function getVTHelpPath() {
  return getSubPath(VTHELP_PATH);
}

export function getVTPoliciesPath() {
  return getSubPath(VTPOLICIES_PATH);
}

export function getPrivacyPath() {
    return getSubPath(PRIVACY_PATH);
}

export function getFeedbackPath() {
    return getSubPath(FEEDBACK_PATH);
}

function getSubPath(path: string) {
    return `${getInfoModulePath()}/${path}`;
}
