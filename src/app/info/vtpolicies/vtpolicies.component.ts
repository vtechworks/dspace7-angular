import { Component } from '@angular/core';

@Component({
  selector: 'ds-vtpolicies',
  templateUrl: './vtpolicies.component.html',
  styleUrls: ['./vtpolicies.component.scss']
})
/**
 * Component displaying the Privacy Statement
 */
export class VTPoliciesComponent {
}
