import { Component } from '@angular/core';
import { ThemedComponent } from '../../shared/theme-support/themed.component';
import { VTPoliciesComponent } from './vtpolicies.component';

/**
 * Themed wrapper for Policies Component
 */
@Component({
  selector: 'ds-themed-vtpolicies',
  styleUrls: [],
  templateUrl: '../../shared/theme-support/themed.component.html',
})
export class ThemedVTPoliciesComponent extends ThemedComponent<VTPoliciesComponent> {
  protected getComponentName(): string {
    return 'VTPoliciesComponent';
  }

  protected importThemedComponent(themeName: string): Promise<any> {
    return import(`../../../themes/${themeName}/app/info/vtpolicies/vtpolicies.component`);
  }

  protected importUnthemedComponent(): Promise<any> {
    return import(`./vtpolicies.component`);
  }

}
