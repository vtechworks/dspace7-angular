import { Component } from '@angular/core';

@Component({
  selector: 'ds-vtpolicies-content',
  templateUrl: './vtpolicies-content.component.html',
  styleUrls: ['./vtpolicies-content.component.scss']
})
/**
 * Component displaying the contents of the vthelp Statement
 */
export class VTPoliciesContentComponent {
}
