import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { VTPoliciesContentComponent } from './vtpolicies-content.component';
import { TranslateModule } from '@ngx-translate/core';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('VTPoliciesContentComponent', () => {
  let component: VTPoliciesContentComponent;
  let fixture: ComponentFixture<VTPoliciesContentComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [TranslateModule.forRoot()],
      declarations: [VTPoliciesContentComponent],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VTPoliciesContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
