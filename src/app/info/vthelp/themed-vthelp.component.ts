import { Component } from '@angular/core';
import { ThemedComponent } from '../../shared/theme-support/themed.component';
import { VTHelpComponent } from './vthelp.component';

/**
 * Themed wrapper for PrivacyComponent
 */
@Component({
  selector: 'ds-themed-vthelp',
  styleUrls: [],
  templateUrl: '../../shared/theme-support/themed.component.html',
})
export class ThemedVTHelpComponent extends ThemedComponent<VTHelpComponent> {
  protected getComponentName(): string {
    return 'VTHelpComponent';
  }

  protected importThemedComponent(themeName: string): Promise<any> {
    return import(`../../../themes/${themeName}/app/info/vthelp/vthelp.component`);
  }

  protected importUnthemedComponent(): Promise<any> {
    return import(`./vthelp.component`);
  }

}
