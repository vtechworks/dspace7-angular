import { Component } from '@angular/core';

@Component({
  selector: 'ds-vthelp-content',
  templateUrl: './vthelp-content.component.html',
  styleUrls: ['./vthelp-content.component.scss']
})
/**
 * Component displaying the contents of the vthelp Statement
 */
export class VTHelpContentComponent {
}
