import { Component } from '@angular/core';

@Component({
  selector: 'ds-vthelp',
  templateUrl: './vthelp.component.html',
  styleUrls: ['./vthelp.component.scss']
})
/**
 * Component displaying the Privacy Statement
 */
export class VTHelpComponent {
}
