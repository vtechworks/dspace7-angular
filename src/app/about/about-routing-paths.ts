import { getInfoModulePath } from '../app-routing-paths';

export const ABOUT_PATH = 'about';

export function getAboutPath() {
  return getSubPath(ABOUT_PATH);
}



function getSubPath(path: string) {
    return `${getInfoModulePath()}/${path}`;
}
