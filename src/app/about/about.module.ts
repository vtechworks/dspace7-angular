import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AboutRoutingModule } from './about-routing.module';
import { AboutComponent } from './about.component'; 
import { ThemedAboutComponent } from './themed-about.component';
import { FeedbackGuard } from '../core/feedback/feedback.guard';

const DECLARATIONS = [
 ThemedAboutComponent,
 AboutComponent
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AboutRoutingModule,
  ],
  declarations: [
    ...DECLARATIONS
  ],
  exports: [
    ...DECLARATIONS
  ],
  providers: [FeedbackGuard]
})
export class AboutModule {
}
