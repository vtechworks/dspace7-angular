import { Component } from '@angular/core';


@Component({
  selector: 'ds-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
/**
 * Component displaying the about Statement
 */
export class AboutComponent { 
}
