import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ThemedAboutComponent } from '../about/themed-about.component';
import { ItemPageResolver } from '../item-page/item-page.resolver';


@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'about',
        component: ThemedAboutComponent,
        data: { title: 'about.title' },
      },
    ])
  ],
  providers: [
    ItemPageResolver,
  ]
})
/**
 * This module defines the routing to the components related to the forgot password components.
 */
export class AboutRoutingModule {
}
