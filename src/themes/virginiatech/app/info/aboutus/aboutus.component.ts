import { Component } from '@angular/core';
import { AboutusComponent as BaseComponent } from '../../../../../app/info/aboutus/aboutus.component';

@Component({
  selector: 'ds-aboutus',
  styleUrls: ['./aboutus.component.scss'],
  templateUrl: './aboutus.component.html'
})

/**
 * Component displaying the About Statement
 */
export class AboutusComponent extends BaseComponent {}
