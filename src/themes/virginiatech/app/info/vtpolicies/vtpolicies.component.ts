import { Component } from '@angular/core';
import { VTPoliciesComponent as BaseComponent } from '../../../../../app/info/vtpolicies/vtpolicies.component';

@Component({
  selector: 'ds-vtpolicies',
  styleUrls: ['./vtpolicies.component.scss'],
  templateUrl: './vtpolicies.component.html'
})


/**
 * Component displaying the Help Statement
 */
export class VTPoliciesComponent extends BaseComponent { }
