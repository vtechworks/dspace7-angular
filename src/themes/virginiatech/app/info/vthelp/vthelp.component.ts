import { Component } from '@angular/core';
import { VTHelpComponent as BaseComponent } from '../../../../../app/info/vthelp/vthelp.component';

@Component({
  selector: 'ds-vthelp',
  styleUrls: ['./vthelp.component.scss'],
  templateUrl: './vthelp.component.html'
})


/**
 * Component displaying the Help Statement
 */
export class VTHelpComponent extends BaseComponent { }
