import { Component, Input } from '@angular/core';
import { FooterComponent as BaseComponent } from '../../../../app/footer/footer.component';

/**
 * Represents the virginiatech custom footer
 */

@Component({
  selector: 'ds-footer',
  styleUrls: ['footer.component.scss'],
  templateUrl: 'footer.component.html'
})
export class FooterComponent extends BaseComponent {
  /*showTopFooter = true;
@Input() name = 'vtlib-footer';*/
}

