import { Component } from '@angular/core';
import { AboutComponent as BaseComponent } from '../../../../app/about/about.component';

@Component({
  selector: 'ds-about',
  styleUrls: ['./about.component.scss'],
  templateUrl: './about.component.html'
})

/**
 * Component displaying the About Statement
 */
export class AboutComponent extends BaseComponent { }
