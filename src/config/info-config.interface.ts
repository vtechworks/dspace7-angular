import { Config } from './config.interface';

export interface InfoConfig extends Config {
  enableEndUserAgreement: boolean;
  enablePrivacyStatement: boolean;
  enableAboutusStatement: boolean;
  enableVTHelpStatement: boolean;
  enableVTPoliciesStatement: boolean;
}
